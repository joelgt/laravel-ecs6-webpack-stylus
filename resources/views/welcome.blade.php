<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1- strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
    <head>
        <meta charset="utf-8">
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> -->
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="robots" content="noodp,noydir" /> 
        <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1"/>
        <meta name="description" content="Una solución diseñada a tu medida para continuar con la vida que quieres tener. Si la vida no se detiene después de los 60, tú tampoco lo hagas."/>
        <meta name="_token" content="{!! csrf_token() !!}"/>

        <title>Web</title>
        
        <!-- Favicon -->
        <link rel="icon" type="image/png" href="{{ url('/favicon.ico') }}">
        
        <!-- Facebook  og -->
        <meta property="og:title" content="Sura - Plan Imparable">
        <meta property="og:description" content="Ten la libertad de diseñar una solución a tu medida para darle el mejor uso a tu fondo de pensiones.">
        <meta property="og:image" content="{{ url('/images/banner_desktop.png') }}">
        <meta property="og:type"  content="website" />
        <meta property="og:url" content="{{ url('') }}">
        
        <!-- Twitter og -->
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@nytimesbits" />
        <meta name="twitter:creator" content="@nickbilton" />

        <!-- Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ url('/css/index.css') }}">
        <link rel="stylesheet" href="{{ url('/css/app.css')}}" />
        
        <!-- IE 9 Validations -->
        <!--[if lt IE 9]>
            <script src="{{ url('/js/html5shiv.js') }}></script>
        <![endif]-->

        <!--[if lt IE 9]>
            <script src="{{ url('/js/respond.src.js') }}"></script>
        <![endif]-->

        <!--[if lt IE 9]>
            <link rel="stylesheet" type="text/css" media="all" href="{{ url('/css/ie8.css') }}"/>
        <![endif]-->

    </head>
    <body>
        
        <!-- Templates  -->

        @yield('content')
        @yield('map')

        <!-- Scripts -->
        <script type="text/javascript" src="{{ url('js/index.min.js') }}"></script>
        <script type="text/javascript">
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });
        </script>
    </body>
</html>
