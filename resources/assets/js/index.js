
// Dependencies vendor
import './vendor/jquery-1.9.1.js';

// Dependencies bower
// import 'bower_components/webcomponentsjs/webcomponents.min.js';

// Dependencies npm
import Hammerjs from 'hammerjs';
import Momentjs from 'moment';
import React from 'react';
import ReactDOM from 'react-dom';

// Modules
import { fromToSendPost } from './modules/EventRequest/index.js';
import { openTermConds } from './modules/EventRequest/index.js';
import { sliceSection } from './modules/sliceSection/index.js';
import { sliderCarousell } from './modules/sliderCarousell/index.js';

import { infoCallback } from './modules/sliceData/index.js';

