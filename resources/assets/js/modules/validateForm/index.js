export function validationTypeFrom () {

    var $inputSelectOptions = document.querySelector('#inputSelectOptions');
    var $inputDoc = document.querySelector('#inputDoc');
    var $inputName = document.querySelector('#inputName');
    var $inputLastName = document.querySelector('#inputLastName');
    var $inputTelefono = document.querySelector('#inputTelefono');
    var $inputEmail = document.querySelector('#inputEmail');
    var $inputDate = document.querySelector('#inputDate');
    var $msgBoxFrom = document.querySelector('#boxMessage');
    
    $msgBoxFrom.innerHTML = '';

    var msg = '';

    $msgBoxFrom.style.color = 'red';


    $('#inputSelectOptions').on('input', function() { 

        var textType = '';

        $inputDoc.style.borderColor = '#dedede';

        $inputDoc.value = '';

        switch($inputSelectOptions.value) {
            case 'dni':
                textType = 'DNI';
                break;
                
            case 'carnet':
                textType = 'Carnet de Extranjería';
                break;

            case 'pasaporte':
                textType = 'Pasaporte';
                break;
        }

        $inputDoc.placeholder = 'Escribe tu ' + textType;

    });

    $('#inputDoc').on('input', function() { 

        var numberLimit = 0;

        if($inputSelectOptions.value === 'dni') {
            numberLimit = 8;
        }

        if($inputSelectOptions.value === 'pasaporte'  || $inputSelectOptions.value === 'carnet') {
            numberLimit = 12;
        }

        $inputDoc.style.borderColor = '#dedede';

        if($inputDoc.value.length >= numberLimit) {
            $inputDoc.value = $inputDoc.value.slice(0,numberLimit); 
            $msgBoxFrom.style.display = 'none';
        }

        if($inputDoc.value.length < numberLimit) {

            msg = 'El campo documento debe tener ' + numberLimit + ' digitos';
            $inputDoc.style.borderColor = '#ed1b2e';

            $msgBoxFrom.style.display = 'block';
            $msgBoxFrom.innerHTML = msg;
        }

        // Validation only number
        var word = $inputDoc.value;

        if(parseInt(word[word.length - 1]) >= 0 === false) {

            word = word.replace(word[word.length - 1],'');

        }

        $inputDoc.value = word;

    });


    $('#inputName').on('input', function() { 
        $inputName.style.borderColor = '#dedede';
        $msgBoxFrom.style.display = 'none';

        var word = $inputName.value;

        // Validation only lettes
        $inputName.value = word;

        var key = word[word.length - 1];
        var letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";

        key = key.toLocaleLowerCase();

         if(letras.indexOf(key) === -1){
             word = word.replace(word[word.length - 1],'')

         }

         $inputName.value = word;

    });

    $('#inputLastName').on('input', function() { 
        $inputLastName.style.borderColor = '#dedede';

        var word = $inputLastName.value;

        // Validation only lettes
        $inputLastName.value = word;

        var key = word[word.length - 1];
        var letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";

        key = key.toLocaleLowerCase();

         if(letras.indexOf(key) === -1){
             word = word.replace(word[word.length - 1],'')

         }

         $inputLastName.value = word;


    });

    $('#inputEmail').on('input', function() { 
        $inputEmail.style.borderColor = '#dedede';
        $msgBoxFrom.style.display = 'none';

    });

    $('#inputTelefono').on('input', function() { 
        $inputTelefono.style.borderColor = '#dedede';

        // Validation only number
        var word = $inputTelefono.value;

        if(parseInt(word[word.length - 1]) >= 0 === false) {

            word = word.replace(word[word.length - 1],'');

        }

        $inputTelefono.value = word;

        $inputTelefono.value = $inputTelefono.value.slice(0,12); 


        if($inputTelefono.value.length < 9) {

            msg = 'El campo telefono no es valido';
            $inputTelefono.style.borderColor = '#ed1b2e';

            $msgBoxFrom.style.display = 'block';
            $msgBoxFrom.innerHTML = msg;

        }

        if($inputTelefono.value.length > 8 && $inputTelefono.value.length < 13) {

            $inputTelefono.style.borderColor = '#dedede';
            $msgBoxFrom.style.display = 'none';
        }

    });

    $('#checkButtonForm').on('input', function() { 

        $msgBoxFrom.style.display = 'none';
        
    });

    $("#inputDate").on("input", function() {
        $(".hide-inputbtns").css('border','1px solid #dedede');
    })

    $(".hide-inputbtns").on("input", function() {
        $(".hide-inputbtns").css('border','1px solid #dedede');
    });



    if($(window).width() > 819) {
        // Event ciclico input date
        var word_date = '';

        setInterval(function () {

            word_date = $('.ws-date').val();

            if($('.ws-date').val().length > 10) {
                $('.ws-date').val(word_date.slice(0,10));
            }

            // Separando
            if($('.ws-date').val().length === 3) {
                $('.ws-date').val()[2] = '/'
                $('.ws-date').val(word_date.slice(0,2) + '/')
            }

            if($('.ws-date').val().length === 6) {
                $('.ws-date').val()[5] = '/'
                $('.ws-date').val(word_date.slice(0,5) + '/')
            }

            var get_day = Number($('.ws-date').val()[0] + $('.ws-date').val()[1])
            var get_mes = Number($('.ws-date').val()[3] + $('.ws-date').val()[4])
            var get_anio = Number($('.ws-date').val()[6] + $('.ws-date').val()[7] + $('.ws-date').val()[8] + $('.ws-date').val()[9])

            // Validate day
            if(get_day > 31) {
                
                $('.ws-date').val()[0] = '3';
                $('.ws-date').val()[1] = '1';
                $('.ws-date').val($('.ws-date').val())
            }

            // Validate mes
            if(get_mes > 12) {

                $('.ws-date').val()[3] = '1';
                $('.ws-date').val()[4] = '2';
                $('.ws-date').val($('.ws-date').val())
            }

            // Validate mes
            if(get_anio > 9999) {

                $('.ws-date').val()[6] = '9';
                $('.ws-date').val()[7] = '9';
                $('.ws-date').val()[8] = '9';
                $('.ws-date').val()[9] = '9';
                $('.ws-date').val($('.ws-date').val())
            }

        }, 100);
    }

}