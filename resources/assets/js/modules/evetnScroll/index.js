export function eventScroll() {
    if($(window).width() < 820) {
        $('.HeaderMain').css('display','none');
        $('.HeaderMain__nav--menu__sandwich-icon').css('display','block');


    } else {
        $('.HeaderMain').css('display','block');
        $('.HeaderMain__nav--menu__sandwich-icon').css('display','none');

    }

    
    // Event Hammer.js => swip
    var sliderBoxDetails = document.querySelector(".PlanImparable");

    function headerEvent() {
        $('.HeaderMain').css('position','fixed');
        $('.HeaderMain').css('background','#ffffff');
        $('.HeaderMain').css('margin-top','0');
        $('.HeaderMain').css('box-shadow','1px 1px 0px rgba(6, 6, 6, 0.23)');

        $('.HeaderMain__nav--logo').css('display','block');
        $('.HeaderMain__nav--menu__list ul li a').css('color','#00aec7');

        $('.HeaderMain').css('display','block');
    }

    // Event scroll menu
    $(window).scroll(function (event) {
        var y = $(this).scrollTop(); //set position from top when to change style in pixels

        if($(window).width() < 820) {
            $('.HeaderMain__nav--menu__sandwich-icon').css('display','block');

        } else {
            $('.HeaderMain__nav--menu__sandwich-icon').css('display','none');

        }
        
        if (y >= 128) {
            $('.HeaderMain').css('position','fixed');
            $('.HeaderMain').css('background','#ffffff');
            $('.HeaderMain').css('margin-top','0');
            $('.HeaderMain').css('box-shadow','1px 1px 0px rgba(6, 6, 6, 0.23)');

            $('.HeaderMain__nav--logo').css('display','block');
            $('.HeaderMain__nav--menu__list ul li a').css('color','#00aec7');

            $('.HeaderMain').css('display','block');


        } else {

            $('.HeaderMain').css('position','absolute');
            $('.HeaderMain').css('background','transparent');
            $('.HeaderMain').css('margin-top','2.4rem');
            $('.HeaderMain').css('box-shadow','0px 0px 0px rgba(0, 0, 0, 0)');
            $('.HeaderMain__nav--menu__sandwich-icon').css('display','none');

            $('.HeaderMain__nav--logo').css('display','none');
            $('.HeaderMain__nav--menu__list ul li a').css('color','#ffffff');

        }
    });
}