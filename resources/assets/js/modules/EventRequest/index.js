
export function fromToSendPost(reqURL, info, method) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: reqURL,
            method: method,
            data: info,
            success: function (result) {
                resolve('result')
            },
            statusCode: {
                422: function(err) {
                    reject('err')
                }
            }
        })
    })
}

export function hello2Function() {
	console.log('hello 2 :D');
}