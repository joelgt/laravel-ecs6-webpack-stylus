export function openTermConds() {
    var $btnOpenTermsCondiciones = document.querySelector('#btnOpenTermsCondiciones');

    // Modal de image
    function modalBox(contentTermsConditionHTML) {
        console.log('Terms and Conditions!!');
        console.log(contentTermsConditionHTML);

        // Get the modal
        var modal = document.getElementById('myModal');
        var modalContent = document.getElementById('myModalContent');

        modal.style.display = "block";

        // Insert Terms and Conditions
        modalContent.innerHTML = '';
        modalContent.innerHTML += contentTermsConditionHTML;

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }
    }

    function f_OpenTerms() {
        console.log('Event click term conds');

        // Get TermsCondicion.html
        $.ajax({
            url: '/terminos_condiciones/index.html',
            method: 'get',
            success: function (result) {
                modalBox(result);
            }
        });

    }

    $('#btnOpenTermsCondiciones').click(f_OpenTerms);
}