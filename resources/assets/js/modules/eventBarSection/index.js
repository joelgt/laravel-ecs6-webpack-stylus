export function EventScrollSections() {
    var $bodyT = document.querySelectorAll('body section');
    var $spanCircle = document.querySelectorAll('.MainVerticalBar a');

    var scrollNow = 0;
    
    $(window).scroll(function (event) {
        var e = $(this).scrollTop(); //set position from top when to change style in pixels

        var positionArrow = 'transform: rotate(180deg)';
        var positionDown = 'transform: rotate(180deg)';


        $spanCircle[0].children[0].style.display = 'none';
        $spanCircle[1].children[0].style.display = 'none';
        $spanCircle[2].children[0].style.display = 'none';
        $spanCircle[3].children[0].style.display = 'none';
        $spanCircle[4].children[0].style.display = 'none';

        $spanCircle[0].style.borderColor = '#0033a0';
        $spanCircle[1].style.borderColor = '#0033a0';
        $spanCircle[2].style.borderColor = '#0033a0';
        $spanCircle[3].style.borderColor = '#0033a0';
        $spanCircle[4].style.borderColor = '#0033a0';

        $('.HeaderMain__nav--menu__list .ListItem a')[0].style.borderBottomColor = 'transparent';
        $('.HeaderMain__nav--menu__list .ListItem a')[1].style.borderBottomColor = 'transparent'; 
        $('.HeaderMain__nav--menu__list .ListItem a')[2].style.borderBottomColor = 'transparent'; 
        $('.HeaderMain__nav--menu__list .ListItem a')[3].style.borderBottomColor = 'transparent'; 


        if(e >= 0 && e <= 600) {
            $spanCircle[0].children[0].style.display = 'block';
            $spanCircle[0].style.borderColor = 'transparent';

            if(scrollNow > e) {
                // Estoy subiendo
                $spanCircle[0].children[0].style.transform = "rotate(180deg)";

            } else {
                // Estoy bajanado
                $spanCircle[0].children[0].style.transform = "rotate(0deg)";
            }
            
            scrollNow = e;

        }

        if(e >= 600 && e <= 1200) {
            $spanCircle[1].children[0].style.display = 'block';
            $spanCircle[1].style.borderColor = 'transparent';

            if(scrollNow > e) {
                // Estoy subiendo
                $spanCircle[1].children[0].style.transform = "rotate(180deg)";

            } else {
                // Estoy bajanado
                $spanCircle[1].children[0].style.transform = "rotate(0deg)";
            }

            scrollNow = e;

            // Marcando la posicion del menu nav
            $('.HeaderMain__nav--menu__list .ListItem a')[0].style.borderBottomColor = '#00aec7'; 

        }

        if(e >= 1200 && e <= 2200) {
            $spanCircle[2].children[0].style.display = 'block';
            $spanCircle[2].style.borderColor = 'transparent';

            if(scrollNow > e) {
                // Estoy subiendo
                $spanCircle[2].children[0].style.transform = "rotate(180deg)";

            } else {
                // Estoy bajanado
                $spanCircle[2].children[0].style.transform = "rotate(0deg)";
            }

            scrollNow = e;

            // Marcando la posicion del menu nav
            $('.HeaderMain__nav--menu__list .ListItem a')[1].style.borderBottomColor = '#00aec7'; 

        }

        if(e >= 2200 && e <= 3500) {
            $spanCircle[3].children[0].style.display = 'block';
            $spanCircle[3].style.borderColor = 'transparent';

            if(scrollNow > e) {
                // Estoy subiendo
                $spanCircle[3].children[0].style.transform = "rotate(180deg)";

            } else {
                // Estoy bajanado
                $spanCircle[3].children[0].style.transform = "rotate(0deg)";
            }

            scrollNow = e;

            // Marcando la posicion del menu nav
            $('.HeaderMain__nav--menu__list .ListItem a')[2].style.borderBottomColor = '#00aec7'; 

        }

        if(e >= 3500) {
            $spanCircle[4].children[0].style.display = 'block';
            $spanCircle[4].style.borderColor = 'transparent';

            if(scrollNow > e) {
                // Estoy subiendo
                $spanCircle[4].children[0].style.transform = "rotate(180deg)";

            } else {
                // Estoy bajanado
                $spanCircle[4].children[0].style.transform = "rotate(0deg)";
            }

            scrollNow = e;

            // Marcando la posicion del menu nav
            $('.HeaderMain__nav--menu__list .ListItem a')[3].style.borderBottomColor = '#00aec7'; 

        }

    });
}