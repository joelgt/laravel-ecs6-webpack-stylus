export function SendDataForm() {
    var $btnSendFormData = document.querySelector('#btnSendFormData');

    function btnSendDataTo() {

        var $checkButtonForm = document.querySelector('#checkButtonForm');

        var $inputSelectOptions = document.querySelector('#inputSelectOptions');
        var $inputDoc = document.querySelector('#inputDoc');
        var $inputName = document.querySelector('#inputName');
        var $inputLastName = document.querySelector('#inputLastName');
        var $inputTelefono = document.querySelector('#inputTelefono');
        var $inputEmail = document.querySelector('#inputEmail');
        var $inputDate = document.querySelector('#inputDate');

        var $msgBoxFrom = document.querySelector('#boxMessage');
        
        $msgBoxFrom.innerHTML = '';

        var msg = '';

        $msgBoxFrom.style.color = 'red';

        var UserData = {
            'optionDoc': $inputSelectOptions.value,
            'numeroDoc': $inputDoc.value,
            'name': $inputName.value,
            'lastName': $inputLastName.value,
            'telefono': $inputTelefono.value,
            'email': $inputEmail.value,
            'date_nacimiento': $inputDate.value
        }

        var docValidNumber = 0;

        // Validando el tipo de documento
        if(UserData.optionDoc === 'dni') {
            docValidNumber = 8;

        } else if (UserData.optionDoc === 'pasaporte' || UserData.optionDoc === 'carnet') {
            docValidNumber = 12;

        } else {
            
            var $inputDoc = document.querySelector('#inputDoc');
            $inputDoc.style.borderColor = 'red';

            msg = 'El tipo de documento, no es valido';
            $msgBoxFrom.innerHTML = msg;
        }


        if(UserData.optionDoc !== '' &&
           UserData.numeroDoc !== '' &&
           UserData.name !== '' &&
           UserData.lastName !== '' &&
           UserData.date_nacimiento !== '' &&
           UserData.telefono !== '' &&
           UserData.email !== '' &&
           UserData.numeroDoc.length === docValidNumber &&
           parseInt(UserData.name) > 0 === false &&
           UserData.email.indexOf('@') !== -1) {

            // Validando data en el cliente
            if($checkButtonForm.checked === true) {

                var $msgBoxFrom1 = document.querySelector('#boxMessage');
                $msgBoxFrom1.style.display = 'block';
                $msgBoxFrom1.style.color = '#0033a0';
                $msgBoxFrom1.innerHTML = 'Enviando...';

                // Cambiando formato de fecha para envio dd/mm/aaaa
                var fecha_nacimiento = new Date($inputDate.value);
                var fecha_nacimiento_month = fecha_nacimiento.getMonth() + 1;   // 0 - 11 *
                var fecha_nacimiento_day = fecha_nacimiento.getDate() + 1;         // 1- 31  *
                var fecha_nacimiento_year = fecha_nacimiento.getFullYear();     // año   *

                UserData.date_nacimiento = fecha_nacimiento_day + '/' + fecha_nacimiento_month + '/' + fecha_nacimiento_year

                // Enviando datos de form al servidor
                fromToSendPost('/data/interesado-response-user', UserData, 'post', function (error, result) {
                    if(error) {

                        $msgBoxFrom1.innerHTML = 'Ocurrio un error: tu email no es valido';

                    } else {

                        $inputDoc.value = '';
                        $inputName.value = '';
                        $inputLastName.value = '';
                        $inputTelefono.value = '';
                        $inputEmail.value = '';

                        var destinosReponse = result.destinosReponse;

                        if(result.status === 'error') {

                            $msgBoxFrom1.innerHTML = 'Ocurrio un error: tu email no es valido';

                        } else {

                            var strAncla= '#solicitar'; //id del ancla
                            $('body,html').stop(true,true).animate({                
                                scrollTop: $(strAncla).offset().top
                            },1400);
                                    

                            $msgBoxFrom1.innerHTML = '';

                            var $msgBoxFrom = document.querySelector('.FormRegister__content__form');
                            var $msgBoxFromThanks = document.querySelector('.FormRegister__content__thanks');

                            $msgBoxFrom.style.display = 'none';
                            $msgBoxFromThanks.style.display = 'block';

                            $('.FormRegister').css('background-position-x','-130px');

                            // event go to back

                            var $btnGoToForm = document.querySelector('#btnGoToForm');

                            function f_goFrom() {
                                $msgBoxFrom.style.display = 'block';
                                $msgBoxFromThanks.style.display = 'none';

                                $('.FormRegister').css('background-position-x','center');

                            }

                            if ($btnGoToForm.addEventListener) {
                                 $btnGoToForm.addEventListener("click", f_goFrom);

                            } else {
                               $btnGoToForm.attachEvent("onclick", f_goFrom);
                            }

                        }

                    }
                });

            } else {

                msg = 'Debes aceptar los terminos y condiciones';

                $msgBoxFrom.style.display = 'block';
                $msgBoxFrom.innerHTML = msg;

            }

        } else {

            var msg = '';

            if(UserData.optionDoc === '') {
                msg = 'El campo tipo documento es obligatorio';
                $inputSelectOptions.style.borderColor = '#ed1b2e';
            
            } else if(UserData.numeroDoc === '') {
                msg = 'El campo dni es obligatorio';
                $inputDoc.style.borderColor = '#ed1b2e';
            
            } else if (UserData.numeroDoc !== '' && UserData.numeroDoc.length !== docValidNumber) {
                msg = 'El campo documento debe tener ' + docValidNumber + ' digitos';
                $inputDoc.style.borderColor = '#ed1b2e';

            } else if (UserData.name === '') {
                msg = 'El campo nombre es obligatorio';
                $inputName.style.borderColor = '#ed1b2e';

            } else if(parseInt(UserData.name) > 0 === true) {
                msg = 'El campo nombre no debe tener numeros';
                $inputName.style.borderColor = '#ed1b2e';

            } else if(UserData.lastName === '') {
                msg = 'El campo apellido es obligatorio';
                $inputLastName.style.borderColor = '#ed1b2e';

            } else if(parseInt(UserData.lastName) > 0 === true) {
                msg = 'El campo apellido no debe tener numeros';
                $inputLastName.style.borderColor = '#ed1b2e';

            } else if(UserData.date_nacimiento === '') {
                 msg = 'El campo Fecha de nacimiento es obligatorio';
                 $(".hide-inputbtns").css('border','1px solid #ed1b2e');

            } else if(UserData.telefono === '') {
                 msg = 'El campo telefono es obligatorio';
                 $inputTelefono.style.borderColor = '#ed1b2e';

            } else if (UserData.email === '') {
                 msg = 'El campo email es obligatorio';
                 $inputEmail.style.borderColor = '#ed1b2e';

            } else if (UserData.email.indexOf('@') === -1) {
                 msg = 'El campo email no es correcto';
                 $inputEmail.style.borderColor = '#ed1b2e';

            } else {
                msg = 'Llena los campos obligatorios';

                $inputTelefono.style.borderColor = '#ed1b2e';
                $inputEmail.style.borderColor = '#ed1b2e';

            }

            var $msgBoxFrom = document.querySelector('#boxMessage');
            $msgBoxFrom.style.display = 'block';
            $msgBoxFrom.innerHTML = msg;

        }

    }

    if ($btnSendFormData.addEventListener) {
         $btnSendFormData.addEventListener("click", btnSendDataTo);

    } else {

        $btnSendFormData.attachEvent("onclick", btnSendDataTo);
    }

}