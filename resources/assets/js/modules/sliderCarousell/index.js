export function eventSliderDetails() {
    var $arrow__left = document.querySelector('#arrow__left');
    var $arrow__right = document.querySelector('#arrow__right');

    function f_arrow__left() {

        var $destinosList = document.querySelectorAll('.Solution__videos .video-item');
        var $sampleItem = document.querySelector('.Solution__videos .video-item-template');

        var valueNow = parseInt($sampleItem.dataset.now);

        var valueNext = valueNow - 1;

        if(valueNext >= 0) {

            // Cambiando Imagen
            $sampleItem.children[0].children[0].src = $destinosList[valueNext].children[0].children[0].src;

            // Cambianado titulo
            $sampleItem.children[1].children[0].innerHTML = $destinosList[valueNext].children[1].children[0].textContent;

            $sampleItem.dataset.now = valueNext;

        } else {
            
            $sampleItem.dataset.now = 2;

            valueNext = 2;

            // Cambiando Imagen
            $sampleItem.children[0].children[0].src = $destinosList[valueNext].children[0].children[0].src;

            // Cambianado titulo
            $sampleItem.children[1].children[0].innerHTML = $destinosList[valueNext].children[1].children[0].textContent;

        }

    }

    function f_arrow__right() {

        var $destinosList = document.querySelectorAll('.Solution__videos .video-item');
        var $sampleItem = document.querySelector('.Solution__videos .video-item-template');

        var valueNow = parseInt($sampleItem.dataset.now);

        var valueNext = valueNow + 1;


        if(valueNext <= 2) {
            // Cambiando Imagen
            $sampleItem.children[0].children[0].src = $destinosList[valueNext].children[0].children[0].src;

            // Cambianado titulo
            $sampleItem.children[1].children[0].innerHTML = $destinosList[valueNext].children[1].children[0].textContent;

            $sampleItem.dataset.now = valueNext;


        } else {
            
            $sampleItem.dataset.now = 0;

            valueNext = 0;

            // Cambiando Imagen
            $sampleItem.children[0].children[0].src = $destinosList[valueNext].children[0].children[0].src;

            // Cambianado titulo
            $sampleItem.children[1].children[0].innerHTML = $destinosList[valueNext].children[1].children[0].textContent;


        }

        
    }

    $('#arrow__left').click(f_arrow__left);
    $('#arrow__right').click(f_arrow__right);

}