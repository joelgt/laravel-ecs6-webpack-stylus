## Requisitos
    - entorno lamp
    - laravel 5.3
    - php >= 5.6

## Pasos de instalación
- clonar el repositorio

- Acualizar las dependencias
    > ` composer install `

- copiar el archivo .env.example a .env
    > ` cp .env.example .env `

- Ejecutar
    > ` php artisan key:generate `

- configurar la conexion a la base de datos en .env

- Ejecutar
    > `php artisan migrate `

## Guia de Desarrollo
    Para modificar o programar nuevos features.

    * Instalar dependencias
    (Requiere tener instalado, nodejs)

    > ` npm install `

- Frontend
    * assets
    ./resources/assets
        - Ejecutar 
        > stylus - Watch css 
            ` npm run build-css `

        > webpack - Watch js
            ` npm run build-js `

        - Los assets se transpilan en ./public, desde alli puedes hacer enlace de .css y .js en el templates
        - Cada comando hace watching, y se puede hacer debugging en la consola, respectivamente.

    * Templates
    ./resources/views

- backend
    * MVC laravel